package ru.ridami.radar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import java.util.ArrayList;
import ru.ridami.radar.java.Aircraft;
import ru.ridami.radar.java.Circle;
import ru.ridami.radar.java.Geometry;
import ru.ridami.radar.java.Radar;

public class LaunchActivity extends AppCompatActivity {
  /**
  * Количество окружностей и самолетов можно поменять в классе radar. {@link ru.ridami.radar.java.Radar }
  * */

  private boolean showBoundaries = false; //показывает границы изображений
  private Radar radar;
  private Geometry geometry;
  private Bitmap icon;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    DisplayMetrics displaymetrics = new DisplayMetrics();
    this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    int screenWidth = displaymetrics.widthPixels > displaymetrics.heightPixels ? displaymetrics.heightPixels : displaymetrics.widthPixels;

    int padding = 10;

    icon = BitmapFactory.decodeResource(getResources(), R.drawable.icon);

    radar = new Radar(screenWidth -  2 * padding - getStatusBarHeight(), (float)icon.getHeight()/icon.getWidth());
    geometry = new Geometry(displaymetrics.widthPixels,displaymetrics.heightPixels);

    setContentView(new DrawView(this));
  }

  public int getStatusBarHeight() {
    int result = 0;
    int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
    if (resourceId > 0) {
      result = getResources().getDimensionPixelSize(resourceId);
    }
    return result;
  }

  class DrawView extends View {
    Paint paint;
    Rect rect;

    public DrawView(Context context) {
      super(context);
      paint = new Paint();
    }

    @Override protected void onDraw(Canvas canvas) {
      super.onDraw(canvas);
      canvas.drawARGB(80, 102, 204, 255);
      drawRadar(canvas);
      drawAirchafts(canvas);
    }

    private void drawRadar(Canvas canvas){
      ArrayList<Circle> circles = radar.getArrayOfCircles();
      paint.setColor(Color.BLACK);
      paint.setStrokeWidth(2);
      paint.setStyle(Paint.Style.STROKE);
      for (Circle circle: circles
      ) {
        drawRadarCircle(canvas, circle);
      }
    }

    private void drawRadarCircle(Canvas canvas, Circle circle) {
      canvas.drawCircle(geometry.centerX, geometry.centerY, circle.radius, paint );
    }

    private void drawAirchafts(Canvas canvas){
      ArrayList<Aircraft> aircrafts = radar.getArrayOfAircraft();
      for (Aircraft aircraft: aircrafts
      ) {
        drawRadarAircrafts(canvas, aircraft);
      }
    }

    private void drawRadarAircrafts(Canvas canvas, Aircraft aircraft) {
      Point startPoint = geometry.toNewCenter(aircraft.pointLeft);
      Point finishPoint = geometry.toNewCenter(aircraft.pointRight);

      rect = new Rect(startPoint.x, startPoint.y, finishPoint.x, finishPoint.y);
      if (showBoundaries) canvas.drawRect(rect, paint);
      canvas.drawBitmap(icon, null, rect, paint);
    }
  }
}
