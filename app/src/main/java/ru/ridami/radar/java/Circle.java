package ru.ridami.radar.java;

import java.util.ArrayList;

/**
 * Created on 29-Jan-18.
 */

public class Circle {
  public int radius;
  private int levelIndex;
  private boolean hasFreeSpace = true;
  private ArrayList<Aircraft> aircrafts;

  Circle(int radius, int levelIndex) {
    this.radius = radius;
    aircrafts = new ArrayList<>();
  }

  boolean hasFreeSpace() {
    return hasFreeSpace;
  }

  void setNoFreePlace() {
    this.hasFreeSpace = false;
  }

  void addAircraft(Aircraft aircraft) {
    aircrafts.add(aircraft);
  }

  ArrayList<Aircraft> getAircrafts() {
    return aircrafts;
  }
}
