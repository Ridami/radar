package ru.ridami.radar.java;

import android.graphics.Point;
import android.util.Log;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created on 29-Jan-18.
 */

public class Radar {
  private final String TAG = getClass().getSimpleName();
  private static final int CIRCLE_COUNT = 15; //[ 1 .. 20 ]
  private static final int AIRCRAFT_COUNT = 100; //[ 0 .. 100 ]
  private static final float AIRCRAFT_PROPORTION = 0.7f; //[ 0 .. 100 ]

  private int minRadius;
  private int radius;

  public ArrayList<Circle> getArrayOfCircles() {
    return arrayOfCircles;
  }

  private ArrayList<Circle> arrayOfCircles = new ArrayList<>();
  private ArrayList<Aircraft> arrayOfAircraft = new ArrayList<>();

  public Radar(int screenWidth, float proportionAircraft) {
    this.radius = screenWidth / 2;
    this.minRadius = radius / CIRCLE_COUNT;
    initCircles();
    createAircrafts(proportionAircraft);
  }

  private void initCircles() {
    for (int i = 1; i <= CIRCLE_COUNT; i++) {
      arrayOfCircles.add(new Circle(this.minRadius * i, i));
      Log.i(TAG, "initCircles: radius = " + arrayOfCircles.get(i - 1).radius);
    }
  }

  private void createAircrafts(float proportional) {
    int widthAir = Math.round(AIRCRAFT_PROPORTION * minRadius);
    int heightAir = Math.round(widthAir * proportional);

    for (int i = 0; i < AIRCRAFT_COUNT; i++) {
      arrayOfAircraft.add(getRandomPointAircraft(widthAir, heightAir));
    }
  }

  private Aircraft getRandomPointAircraft(int widthAir, int heightAir) {
    int x, y;
    Random rand = new Random();
    Aircraft aircraft;
    Point newPoint;
    Circle currentLevel;
    int index;
    int angle = 1;

    x = 2 * rand.nextInt(radius) - radius;
    y = 2 * rand.nextInt(radius) - radius;
    int hypotenuse = getHypotenuse(x, y);

    if (hypotenuse > this.radius) {
      aircraft = getRandomPointAircraft(widthAir, heightAir);
    } else {
      //размещаем картинку на центральной окружности диска
      index = CIRCLE_COUNT - (this.radius - hypotenuse) / minRadius - 1;
      if (index < 0 ) index = 0;
      currentLevel = arrayOfCircles.get(index);
      newPoint = getPointForCenterOfImage(x, y, hypotenuse,  currentLevel.radius);
      //Создаем контейнер для картинки и ищем для него свободное место.
      //если свободных мест нет, размещаем на случайную позицию.
      aircraft = new Aircraft(newPoint, widthAir, heightAir);
      if (currentLevel.hasFreeSpace()) {
        while (!isThePlaceFree(aircraft, index) && angle <= 259) {
          aircraft.moveByAngle(angle);
          angle++;
        }
        if (angle == 259) currentLevel.setNoFreePlace();
      }
      currentLevel.addAircraft(aircraft);
    }
    return aircraft;
  }

  private Point getPointForCenterOfImage(int x, int y, float hypotenuse, int levelRadius) {

    float sin = (float) y / hypotenuse;
    float cos = (float) x / hypotenuse;
    float newHypotenuse = levelRadius - minRadius / 2;

    x = Math.round(cos * newHypotenuse);
    y = Math.round(sin * newHypotenuse);
    return new Point(x, y);
  }

  private boolean isThePlaceFree(Aircraft newAir, int index) {
    ArrayList<Aircraft> airsOfLevel = arrayOfCircles.get(index).getAircrafts();
    for (Aircraft oldAir : airsOfLevel) {
      if (isIntersects(newAir, oldAir)) {
        Log.i(TAG, "isThePlaceFree: not place not free");
        return false;
      }
    }
    Log.i(TAG, "the place free");
    return true;
  }

  private boolean isIntersects(Aircraft newAir, Aircraft oldAir) {
    return !(newAir.pointRight.y >= oldAir.pointLeft.y
        || newAir.pointLeft.y <= oldAir.pointRight.y
        || newAir.pointRight.x <= oldAir.pointLeft.x
        || newAir.pointLeft.x >= oldAir.pointRight.x);
  }

  private int getHypotenuse(int x, int y) {
    return (int) Math.round(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
  }

  public ArrayList<Aircraft> getArrayOfAircraft() {
    return arrayOfAircraft;
  }
}
