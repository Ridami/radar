package ru.ridami.radar.java;

import android.graphics.Point;

/**
 * Created on 29-Jan-18.
 */

public class Geometry {
  private Point centerPoint;
  private int screeenWidth;
  private int screenHeight;
  public int centerY;
  public int centerX;

  public Geometry(int screenWidth, int screenHeight) {
    this.screeenWidth = screenWidth;
    this.screenHeight = screenHeight;
    this.centerY = Math.round(screenHeight/2)-10;
    this.centerX = Math.round(screenWidth/2);

    centerPoint = new Point(centerX , centerY);
  }

  // функция пересчета в экранные координаты
  // из координат радара ( по отношению к центру экрана)
  // и разворот оси ординат
  public Point toNewCenter(Point point){
    int newX = centerPoint.x + point.x;
    int newY = centerPoint.y - point.y;
    return new Point(newX, newY);
  }

}
