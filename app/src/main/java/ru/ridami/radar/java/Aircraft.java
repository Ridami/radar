package ru.ridami.radar.java;

import android.graphics.Point;

/**
 * Created on 29-Jan-18.
 */

public class Aircraft {
  // координаты вершин прямоугольника с изображением.
  public Point pointLeft, pointRight;
  public int height;
  public int width;
  private Point centerPoint;

  public Aircraft(Point startPoint, int width , int height) {
    this.centerPoint = startPoint;
    this.width = width;
    this.height = height;

    calculateEdlePoints();
  }

  public void moveByAngle( int angleDegrees){
    double x, y;
    double angleRad;
    angleRad = Math.toRadians(angleDegrees);
    x = centerPoint.x * Math.cos(angleRad) + centerPoint.y * Math.sin(angleRad);
    y = centerPoint.x * Math.sin(angleRad) - centerPoint.y * Math.cos(angleRad);
    this.centerPoint.set((int)Math.round(x), (int) Math.round(y));
    calculateEdlePoints();
  }

  private void calculateEdlePoints(){
    this.pointLeft  = new Point(centerPoint.x - width/2, centerPoint.y + height/2);
    this.pointRight = new Point(centerPoint.x + width/2, centerPoint.y - height/2);

  }

  public Point getCenterPoint() {
    return centerPoint;
  }
}
